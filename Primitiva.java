public class Primitiva {
	public static byte genAleatorio() {
		return (byte) (Math.random()*49+1);
	}
	public static void main(String[] args) {
		byte a = genAleatorio();
		byte b = genAleatorio();
		byte c = genAleatorio();
		byte d = genAleatorio();
		byte e = genAleatorio();
		byte f = genAleatorio();

		while (a==b) b = genAleatorio();
		while ((a==c) || (b==c)) c = genAleatorio();
		while ((a==d) || (b==d) || (c==d) ) d = genAleatorio();
		while ((a==e) || (b==e) || (c==e) || (d==e) ) e = genAleatorio();
		while ((a==f) || (b==f) || (c==f) || (d==f) || (e==f) ) f = genAleatorio();

		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);

	
	}
}