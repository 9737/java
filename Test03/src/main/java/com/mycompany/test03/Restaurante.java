/*
 10. (Ejercicio de dificultad alta) El restaurante mejicano de Israel cuya 
especialidad son las papas con chocos nos pide diseñar un método con el que se 
pueda saber cuántos clientes pueden atender con la materia prima que tienen en 
el almacén. El método recibe la cantidad de papas y chocos en kilos y devuelve 
el número de clientes que puede atender el restaurante teniendo en cuenta que por 
cada tres personas, Israel utiliza un kilo de papas y medio de chocos.
11. Modifica el programa anterior creando una clase que permita almacenar los kilos de papas y chocos del
restaurante. Implementa los siguientes métodos:
- public void addChocos(int x). Añade x kilos de chocos a los ya existentes.
- public void addPapas(int x). Añade x kilos de papas a los ya existentes.
- public int getComensales(). Devuelve el número de clientes que puede atender el restaurante (este es el
método anterior).
- public void showChocos(). Muestra por pantalla los kilos de chocos que hay en el almacén.
- public void showPapas(). Añade Muestra por pantalla los kilos de papas que hay en el almacén.

 */
package com.mycompany.test03;

/**
 *
 * @author philipp
 */
public class Restaurante {
    private double chocos, 
                   papas;
    public void addChocos(int x) {
        this.chocos += (double) x;
    }
    public void addPapas(int x) {
        this.papas += (double) x;
    }
    public int getComensales() {
        return (int) numPersonas(chocos,papas);
    }
    public void showChocos() {
        System.out.println("Actualmente hay "+chocos+"kg de chocos en el almacen.");
    }
    public void showPapas() {
        System.out.println("Actualmente hay "+papas+"kg de papas en el almacen.");
    }
    
    
    public double numPersonas(double chocos, double papas) {
        double n=0;
        while (chocos>=(0.5/3) && papas>=(1.0/3)) {
           n++;
           chocos-=(0.5/3);
           papas-=(1.0/3);
        }
        return n;
    }

}
