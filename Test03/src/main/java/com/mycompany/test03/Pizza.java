/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test03;

/**
 *
 * @author daw
 */
public class Pizza {
    private String tipo;
    private String tamanio;
    private String estado;
    private static int pedidas;
    private static int servidas;
    public Pizza(String tipo, String tamanio) {
        estado = "pedida";
        this.tipo = tipo;
        this.tamanio = tamanio;
        pedidas++;
    }
    
    public void sirve() {
        if (estado=="servida") {
            System.out.println("esa pizza ya se ha servido");
        }
        else {
            this.estado = "servida";
            servidas++;
        }
    }
    public String toString() {
        return "pizza "+tipo+" "+tamanio+", "+estado;
    }
    
    public static int getTotalPedidas() {
        return pedidas;
    }
    
    public static int getTotalServidas() {
        return servidas;
    }
}
