public class Letra2 {
	public static char genAleatorio() {
		return (char) (Math.random()*26+65);
	}

public static char genAleatorio(int inf, int sup) {
		return (char) (Math.random()*(sup-inf)+inf);
	}

	public static void main(String[] Arrgh ) {
		char a = genAleatorio(65,91);
		char b = genAleatorio(65,91);
		char c = genAleatorio(65,91);
		char d = genAleatorio(65,91);
		char e = genAleatorio(65,91);
		char f = genAleatorio(65,91);

		while (a==b) b = genAleatorio(65,91);
		while ((a==c) || (b==c)) c = genAleatorio(65,91);
		while ((a==d) || (b==d) || (c==d) ) d = genAleatorio(65,91);
		while ((a==e) || (b==e) || (c==e) || (d==e) ) e = genAleatorio(65,91);
		while ((a==f) || (b==f) || (c==f) || (d==f) || (e==f) ) f = genAleatorio(65,91);

		//comprobar si letra es vocal
		//A = 65
		//E = 69
		//I = 73
		//O = 79
		//U = 85

		if ((a == 65) || (a==69) || (a==73) || (a==79) || (a==85))
			System.out.println(a + " es un vocal.");
		else System.out.println(a + " es un consonante.");
		if ((b == 65) || (b==69) || (b==73) || (b==79) || (b==85))
			System.out.println(b + " es un vocal.");
		else System.out.println(b + " es un consonante.");
		if ((c == 65) || (c==69) || (c==73) || (c==79) || (c==85))
			System.out.println(c + " es un vocal.");
		else System.out.println(c + " es un consonante.");
		if ((d == 65) || (d==69) || (d==73) || (d==79) || (d==85))
			System.out.println(d + " es un vocal.");
		else System.out.println(d + " es un consonante.");
		if ((e == 65) || (e==69) || (e==73) || (e==79) || (e==85))
			System.out.println(e + " es un vocal.");
		else System.out.println(e + " es un consonante.");
		if ((f == 65) || (f==69) || (f==73) || (f==79) || (f==85))
			System.out.println(f + " es un vocal.");
		else System.out.println(f + " es un consonante.");


		System.out.println("Lista antes de ordenar:");
		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);

		//ordenar ascendiente bubble sort
		char temp;
		if (a>b) {temp=b; b=a; a=temp;}
		
		if (a>c) {temp=c; c=a; a=temp;}
		if (b>c) {temp=c; c=b; b=temp;}

		if (a>d) {temp=d; d=a; a=temp;}
		if (b>d) {temp=d; d=b; b=temp;}
		if (c>d) {temp=d; d=c; c=temp;}

		if (a>e) {temp=e; e=a; a=temp;}
		if (b>e) {temp=e; e=b; b=temp;}
		if (c>e) {temp=e; e=c; c=temp;}
		if (d>e) {temp=e; e=d; d=temp;}

		if (a>f) {temp=f; f=a; a=temp;}
		if (b>f) {temp=f; f=b; b=temp;}
		if (c>f) {temp=f; f=c; c=temp;}
		if (d>f) {temp=f; f=d; d=temp;}		
		if (e>f) {temp=f; f=e; e=temp;}	

		System.out.println("Lista después de ordenar:");
		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);

	}
}