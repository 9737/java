/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author philipp
 */
public class E05temperaturas {
    private int[][] temperatura;
    public E05temperaturas(String nombreEntrada) throws FileNotFoundException {
        leerDatos(nombreEntrada);
    }
    
    public void leerDatos(String nombreEntrada) throws FileNotFoundException {
        // leer datos desde fichero

        FileInputStream fis= new FileInputStream(nombreEntrada);
        System.setIn(fis);
        Scanner teclado = new Scanner(System.in);
        
        temperatura= new int[12][];
        temperatura[0] = new int[31];
        temperatura[1] = new int[28];
        temperatura[2] = new int[31];
        temperatura[3] = new int[30];
        temperatura[4] = new int[31];
        temperatura[5] = new int[30];
        temperatura[6] = new int[31];
        temperatura[7] = new int[31];
        temperatura[8] = new int[30];
        temperatura[9] = new int[31];
        temperatura[10] = new int[30];
        temperatura[11] = new int[31];
        
        for (int m=0;m<12;m++) {
            for (int d=0;d<temperatura[m].length;d++) {
                temperatura[m][d] = teclado.nextInt();
            }
        }
        
    }

    public int getTemp(int mes,int dia) {
        return temperatura[mes][dia];
    }
    public double tempMedia(int mes) {
        double media = 0;
        for (int d=0;d<temperatura[mes].length;d++)
        {
            media += temperatura[mes][d];
        }
        media /= temperatura[mes].length;
        return media;
    }
    
    public int[] extremosMes(int mes) {
        int[] res = new int[2];
        int min =temperatura[mes][0];
        int max =temperatura[mes][0];
        res[0] = 0;
        res[1] = 0;
        
        for (int d=1;d<temperatura[mes].length;d++)
        {
            if (temperatura[mes][d] < min) {
                min = temperatura[mes][d];
                res[0] = d;
            }
            if (temperatura[mes][d] > max) {
                max = temperatura[mes][d];
                res[1] = d;
            }
        }
        return res;
    }
    public int[] extremosAnio() {
        int res[] = new int[4];
        int min = temperatura[0][0];
        int max = temperatura[0][0];
        res[0] = 0;
        res[1] = 0;
        res[2] = 0;
        res[3] = 0;
        for (int m=0;m<12;m++) {
            for (int d=0;d<temperatura[m].length;d++) {
                if (temperatura[m][d] < min) {
                    min = temperatura[m][d];
                    res[0] = m;
                    res[1] = d;
                }
                if (temperatura[m][d] > max) {
                    max = temperatura[m][d];
                    res[2] = m;
                    res[3] = d;
                }
            }
        }
        return res;
    }

    
    
    public static void main(String args[]) throws FileNotFoundException {
        E05temperaturas t1 = new E05temperaturas("/tmp/e05.ExtraArraysDatosTemperaturas.txt");
        for (int mes=0;mes<12;mes++) {
            System.out.printf("La temperatura media del mes %d era %.2f grados\n",mes+1,t1.tempMedia(mes));
            int[] extremosMes = t1.extremosMes(mes);
            System.out.printf("El día más caluroso fue el %d con %d grados y el día más frío el %d con %d grados.\n", 
                    extremosMes[1]+1, 
                    t1.getTemp(mes, extremosMes[1]),
                    extremosMes[0]+1,
                    t1.getTemp(mes, extremosMes[0]))
                    ;
        }
        int[] extremos = t1.extremosAnio();
        System.out.printf("Los extremos del año eran %d grados el %d de %d, y %d grados el %d de %d\n", 
                t1.getTemp(extremos[0],extremos[1]), 
                extremos[1]+1, 
                extremos[0]+1,
                t1.getTemp(extremos[2],extremos[3]), 
                extremos[3]+1, 
                extremos[2]+1
                );
        
    }

}
