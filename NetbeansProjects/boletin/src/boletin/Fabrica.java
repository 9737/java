/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author philipp
 */
public class Fabrica {    
    final static int CENTROS = 4;
    final static int MODELOS = 4;
    final static double PRECIOS[] = {1.5, 1.75, 2.42, 2.6};
    
    // filas modelos, columnas centros
    private int[][] unidadesVendidas;
    
    public Fabrica(String nombreEntrada) throws FileNotFoundException {
        leerDatos(nombreEntrada);
    }
    
    public void leerDatos(String nombreEntrada) throws FileNotFoundException {
        this.unidadesVendidas = new int[MODELOS][CENTROS];
        // leer datos hasta elemento -1

        //desde fichero

        FileInputStream fis= new FileInputStream(nombreEntrada);
        System.setIn(fis);

        int mod,fab;

        Scanner teclado = new Scanner(System.in);

        while (true) {
            mod=teclado.nextInt();
            if (mod==-1) {
                break;
            }
            else {
                fab=teclado.nextInt();
                unidadesVendidas[mod][fab]++;

            }
        }
    }

    
    public double[] ventasTotal() {
        int unidadesTotal = 0;
        double ventasTotal = 0;
        for (int mod=0;mod<MODELOS;mod++) {
            for (int cent=0;cent<CENTROS;cent++) {
                unidadesTotal += unidadesVendidas[mod][cent];
                ventasTotal += unidadesVendidas[mod][cent]*PRECIOS[mod];
            }
        }
        System.out.printf("En total se vendieron %d unidades por %5.2f millones de pesetas.\n", unidadesTotal, ventasTotal);
        double[] res = {unidadesTotal, ventasTotal};
        return res;
    }

    public void ventasPorCentro() {
        
        for (int cent=0;cent<CENTROS;cent++) {
            int unidadesTotal = 0;
            double ventasTotal = 0;
            for (int mod=0;mod<MODELOS;mod++) {
                unidadesTotal += unidadesVendidas[mod][cent];
                ventasTotal += unidadesVendidas[mod][cent]*PRECIOS[mod];
            }
            System.out.printf("En centro %d se vendieron %d unidades por %5.2f millones de pesetas.\n", cent, unidadesTotal, ventasTotal);
        }
    }

    
    public void porcentajeCentro() {
        
        double[] totalGlobal = this.ventasTotal();
        for (int cent=0;cent<CENTROS;cent++) {
            int unidadesTotal = 0;
            double ventasTotal = 0;
            for (int mod=0;mod<MODELOS;mod++) {
                unidadesTotal += unidadesVendidas[mod][cent];
                ventasTotal += unidadesVendidas[mod][cent]*PRECIOS[mod];
            }
            System.out.printf("En centro %d se vendieron %d unidades por %5.2f millones de pesetas.\n", cent, unidadesTotal, ventasTotal);
            System.out.printf("Eso corresponde a %4.2f%% de las unidades vendidas y %4.2f%% de las ventas.\n", (double) unidadesTotal/totalGlobal[0]*100, ventasTotal/totalGlobal[1]*100);
        }
    }
    
    public void porcentajeModelosCentros() {
        // sobre unidades totales vendido por la empresa
        double[] totalGlobal = this.ventasTotal();
        double[][] pctMod = new double[MODELOS][CENTROS];
        double[][] pctPesetas = new double[MODELOS][CENTROS];
        for (int mod=0;mod<MODELOS;mod++) {
            for (int cent=0;cent<CENTROS;cent++) {
                pctMod[mod][cent] = unidadesVendidas[mod][cent]/totalGlobal[0]*100;
                pctPesetas[mod][cent] = unidadesVendidas[mod][cent]*PRECIOS[mod]/totalGlobal[1]*100;
            }
        }
        System.out.println("tanto por ciento de unidades vendidos:");
        System.out.println(boletin.arrays.UtilidadesMatrices.devuelveArray(pctMod));
        System.out.println();
        System.out.println("tanto por ciento de ventas en pesetas:");
        System.out.println(boletin.arrays.UtilidadesMatrices.devuelveArray(pctPesetas));
    }


    public static double[][] precios(int[][] ventas, double[] precioModelo) {
        double[][] res = new double[ventas.length][ventas[0].length];
        for (int i=0;i<ventas.length;i++) {
            for (int j=0;j<ventas[0].length;j++) {
                res[i][j] = ventas[i][j]*precioModelo[i];
            }
        }
        return res;
    }
    
    public static double[][] porcentajeUnidades(int[][] ventas) {
        // calcular total
        double total = 0.0;
        int i;
        for (i=0;i<ventas.length;i++) {
            for (int j=0;j<ventas[0].length;j++) {
                total += ventas[i][j];
            }
        }
        // calcular porcentajes
        double[][] res = new double[ventas.length][ventas[0].length];
        for (i=0;i<ventas.length;i++) {
            for (int j=0;j<ventas[0].length;j++) {
                res[i][j] = ventas[i][j] / total;
            }
        }
        return res;
    }
    
    public static double ventasTotales(int[][] ventas) {
        int total = 0;
        for (int i=0;i<ventas.length;i++) {
            for(int j=0;j<ventas[0].length;j++) {
                total += ventas[i][j];
            }
        }
        return total;
    }
    
    public static double[] porcentajeUnidadesCentros(int[][] ventas) {
    
        // calcular total de ventas en cada centro
        // y ventas totales
        
        int[] totalVentasPorCentro = new int[4];
        for (int centro=0;centro<4;centro++) {
            
            for (int mod=0;mod < 4;mod++) {
                totalVentasPorCentro[centro]+=ventas[mod][centro];
            }
        }
        
        int total = 0;
        for (int i=0; i<totalVentasPorCentro.length;i++) {
            total += totalVentasPorCentro[i];
        }
        
        int[] ventasPorCentro = new int[4];
        for (int i=0;i<4;i++) {
            ventasPorCentro[i] =0;
            for (int j=0;j<4;j++) {
                ventasPorCentro[i] += porcentajeUnidades(ventas)[j][i];
            }
        }
        
        double[] porcentajePorCentro = new double[4];
        for (int i=0;i<4;i++) {
            porcentajePorCentro[i] = ventasPorCentro[i] / total;
        }
        return porcentajePorCentro;
    }

    public static void main(String args[]) throws FileNotFoundException{
        
        Fabrica f1 = new Fabrica("/tmp/datos_fabrica.txt");
        System.out.println(boletin.arrays.UtilidadesMatrices.devuelveArray(f1.unidadesVendidas));
        f1.ventasTotal();
        f1.ventasPorCentro();
        f1.porcentajeCentro();
        f1.porcentajeModelosCentros();
        
    }
}