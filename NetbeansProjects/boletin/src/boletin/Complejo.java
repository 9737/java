/*
13. Realizad una clase Complejo que implemente los métodos suma,
resta, producto, división y potencia   de   números   complejos,
y   un   método   numComplejos   que   nos   devuelva   el   número
de objetos Complejo creados en nuestra aplicación.
 */
package com.mycompany.test03;

/**
 *
 * @author daw
 */
public class Complejo {
    private double Re;
    private double Im;
    private static int numCreados;
    
    public Complejo() {
        
    }
    public Complejo(double Re, double Im) {
        this.Re = Re;
        this.Im = Im;
    }
    public Complejo suma(Complejo c2) {
        // (a,b) + (c,d) = (a+b, b+d)
        return new Complejo(this.Re+c2.Re, this.Im+c2.Im);
    }
    public Complejo resta(Complejo c2) {
        return new Complejo(this.Re-c2.Re, this.Im-c2.Im);
    }
    
    public Complejo producto(Complejo c2) {
        // (a,b) * (c,d) = (ac - bd, ad+bc)
        return new Complejo(this.Re * c2.Re - this.Im * c2.Im, 
                            this.Re * c2.Im + this.Re * c2.Re);
    }
    
    public Complejo division(Complejo c2) {
        // (a,b) / (c,d) = (ac + bd, bc - ad) / (c*c + d*d) =
        // (ac + bd)/(c*c + d*d) , (bc - ad) / (c*c + d*d)
        return new Complejo(
                   (this.Re * c2.Re + this.Im * c2.Im) /
                           (c2.Re*c2.Re + c2.Im * c2.Im),
                (this.Im * c2.Re - this.Re * c2.Im) /
                        (c2.Re*c2.Re + c2.Im * c2.Im));
    }
    public Complejo potencia(Complejo c1, int exp) {
        return new Complejo();
    }
    
    public static int numComplejos() {
        return numCreados;
    }
   
    public String toString() {
        return "Real: "+Re+" Imaginario: "+Im;
    }
}
