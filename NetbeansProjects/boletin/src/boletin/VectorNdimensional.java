/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;

/**
 *
 * @author daw
 */
public class VectorNdimensional {
    private static int vectoresCreados;
    private double[] x;
    
    public VectorNdimensional() {
    }
    public VectorNdimensional(double[] x) {
        this.x = new double[x.length];
        System.arraycopy(x, 0, this.x, 0, x.length);
        vectoresCreados++;
    }
    public VectorNdimensional(VectorNdimensional v) {
        this();
        System.arraycopy(v.x, 0, this.x, 0, v.x.length);
    }
    public static int getNumVectores() {
        return vectoresCreados;
    }
    public VectorNdimensional suma(VectorNdimensional v2) {
        double[] y = new double[x.length];
        for (int i =0; i < this.x.length; i++) {
            y[i] = this.x[i] + v2.x[i];
        }
        return new VectorNdimensional(y);
    }
    public VectorNdimensional resta(VectorNdimensional v2) {
        double[] y = new double[x.length];
        for (int i =0; i < this.x.length; i++) {
            y[i] = this.x[i] - v2.x[i];
        }
        return new VectorNdimensional(y);
    }
    public double prodEscalar(VectorNdimensional v2) {
        double suma = 0;
        for (int i=0; i < this.x.length; i++) {
            suma += this.x[i] * v2.x[i];
        }
        return suma;
    }
    public VectorNdimensional prodVectorial(VectorNdimensional v2) {
        
        double ux=this.x[1], uy=this.x[2], uz=this.x[3], vx=v2.x[1], vy=v2.x[2], vz=v2.x[3];
        
        
        // return new VectorTridimensional(uy*vz-uz*vy,uz*vx-ux*vz,ux*vy-uy*vx);

        return null;
    }
    
    @Override
    public String toString() {
        String res = "[";
        for (double dim: this.x) {
            res +=dim+" ";
        }
        res+="]";
        return res;
    }
    
    public double modulo() {
        double suma = 0;
        for (int i=0; i<x.length; i++) {
            suma+=x[i]*x[i];
        }
        return Math.sqrt(suma);
    }
}
