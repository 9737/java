
package boletin;
import static boletin.arrays.UtilidadesMatrices.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author philipp
 */
public class Test {
    
    public static int[] sumaArray(int[] a, int[] b) {
        int res[] = new int[a.length];
        for (int i=0; i<a.length; i++) {
            res[i] = a[i] + b[i];
        }
        return res;
    }
    
    public static void main(String args[]) {
        /*
        Restaurante r1 = new Restaurante();
        double chocos = 4.2;
        double papas = 5.5;
        System.out.println("Israel tiene "+chocos+" Chocos y "+papas+"kg papas. ");
        System.out.println("Con eso él puede servir a "+r1.numPersonas(chocos, papas)+" personas");
        
        Restaurante r2 = new Restaurante();
        r2.addChocos(7);
        r2.addPapas(4);
        r2.showChocos();
        r2.showPapas();
        System.out.println("Con estos se pueden servir "+r2.getComensales()+" comensales");

        */
        /*
        Complejo c1 = new Complejo(3.0,4.0);
        Complejo c2 = new Complejo(2.4,1.5);
        System.out.println(c1.suma(c2));
        
        
        int a[] = {1, 2, 3};
        int b[] = {3, 4, 5};
        for (int i: sumaArray(a,b)) {
            System.out.println(i);
        }    
        */
        System.out.println("Expectamos 36: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("8567 7065 36 5011468036"));
        System.out.println("Expectamos 71: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("8530 9509 71 6838424992"));
        System.out.println("Expectamos 18: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("3540 8921 18 0211963332"));
        System.out.println("Expectamos 69: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("9117 1459 69 3852516469"));
        System.out.println("Expectamos 39: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("4234 8424 39 9658967099"));
        System.out.println("Expectamos 09: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("6874 2998 09 7114952328"));
        System.out.println("Expectamos 11: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("8804 4544 11 5170256696"));
        System.out.println("Expectamos 00: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("8946 4598 00 8867795167"));
        System.out.println("Expectamos 54: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("1908 5365 54 1190077642"));
        System.out.println("Expectamos 38: ");
        System.out.println(boletin.arrays.UtilidadesMatrices.controlCCC("1282 9380 38 8141432015"));
        
        
        /*
        int[] a= {1,2,3,4,5};
        int[] b= {1,5,7,8};
        System.out.println(boletin.arrays.UtilidadesMatrices.devuelveArray(
                boletin.arrays.UtilidadesMatrices.unirMatrices(a,b)));

        double[][] a = {{3.0,6.0,9.0},{11.0,15.0,17.0},{9.0,6.0,4.0}};
        System.out.println(boletin.arrays.UtilidadesMatrices.devuelveArray(a));
        boletin.arrays.UtilidadesMatrices.diagMatriz(a);
        
        double[] a= {17,8,3,40,50,70,100,1};
        
        System.out.println("Antes de ordenar: "+devuelveArray(a));
        System.out.println("Despues de ordenar: "+devuelveArray(burbuja(a)));
        */
    }
}
