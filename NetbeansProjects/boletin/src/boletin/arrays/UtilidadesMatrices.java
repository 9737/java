package boletin.arrays;
public class UtilidadesMatrices {
	public static int[] suma(int[] a, int[]b) {
		if (a.length != b.length) {
			return null;
		}
		int[] c = new int[a.length];
		for (int i=0;i<a.length;i++) {
			c[i] = a[1] + b[i];
		}
		return c;
	}
	
        /* arrayBidSuma que recibe dos arrays bidimensionales de números enteros y
devuelve un array bidimensional con la suma de los elementos de la misma posición de ambos.
*/
        
        public static int[][] arrayBidSuma(int[][] a, int [][] b) {
            if (a.length!=b.length || a[0].length != b[0].length) {
                return null;
            }
            int[][] c = new int[a.length][a[0].length];
            
            for (int fila=0;fila<a.length;fila++) {
                for (int col=0; col<a[0].length;col++) {
                    c[fila][col] = a[fila][col] + b[fila][col];
                }
            }
            return c;
        }
        
        
        /* 2.- Método que devuelva los dígitos de control de una cuenta bancaria. */
        public static String controlCCC(String numero) {
            // tomar numero y splitlo por espacios
            String[] partes=numero.split(" ");
            //EEEE OOOO DD NNNNNNNNNN
            // 00 EEEE OOOO => primer dígito
            
            String primeraDecena = "00"+partes[0]+partes[1];
            String segundaDecena = partes[3];
            
            // NNNNNNNNNN => segundo dígito
            final int[] FACTORES = {1,2,4,8,5,10,9,7,3,6};
            int suma1=0,suma2 = 0;
            
            // recorrer las decenas de la izquierda a la derecha,
            // multiplicando con la posicion correspondiente de FACTORES
            for (int i=0;i<10;i++) {
                
                // for*int i=0; i<10;i++) {
                // Math.pow(2,i)%11;
                
                int digitoPrimero = Character.getNumericValue(primeraDecena.charAt(i));
                suma1 += digitoPrimero * Math.pow(2,i)%11;
                int digitoSegundo = Character.getNumericValue(segundaDecena.charAt(i));
                suma2 += digitoSegundo * Math.pow(2,i)%11;
            }
            int r1 = suma1 % 11;
            int r2 = suma2 % 11;
            int c1, c2;
            c1 = 11-r1;
            c2 = 11-r2;
            if (c1==10) c1=1;
            else if (c1==11) c1=0;
            if (c2==10) c2=1;
            if (c2==11) c2=0;
            
            return Integer.toString(c1)+Integer.toString(c2);
        }
        
        
        public static void swap(int[] v,int i,int j)
        {
            int aux=v[i];
            v[i]=v[j];
            v[j]=aux;
        }
        
        public static void swap(double[] v,int i,int j)
        {
            double aux=v[i];
            v[i]=v[j];
            v[j]=aux;
        }
        
        public static int[] burbuja(int[] vector) {
            int i,j;
            int aux;
            i=0;
            boolean ordenado=true;
            //for(i=0;i<vector.length-1;i++)
            do
            {
                ordenado=true;
                for(j=0;j<vector.length-i-1;j++)
                {
                    if(vector[j]>vector[j+1])
                    {
                        swap(vector,j,j+1);
                        ordenado=false;
                    }
                }
                i++;
            } while (ordenado==false && (i<vector.length-1));
            return vector;
        }
        
        public static double[] burbuja(double[] vector) 
        {
            int i,j;
            double aux;
            i=0;
            boolean ordenado=true;
            //for(i=0;i<vector.length-1;i++)
            do
            {
                ordenado=true;
                for(j=0;j<vector.length-i-1;j++)
                {
                    if(vector[j]>vector[j+1])
                    {
                        swap(vector,j,j+1);
                        ordenado=false;
                    }
                }
                i++;
            } while (ordenado==false && (i<vector.length-1));    
            return vector;
        }
        
        
        public static double[] burbuja(double[] vector, boolean direccion) 
        {
            int i,j;
            double aux;
            i=0;
            boolean ordenado=true;
            
            do
            {
                ordenado=true;
                for(j=0;j<vector.length-i-1;j++)
                {
                    if (direccion == true) {
                        if(vector[j]>vector[j+1])
                        {
                            swap(vector,j,j+1);
                            ordenado=false;
                        }
                    }
                    else {
                        if(vector[j]<vector[j+1])
                        {
                            swap(vector,j,j+1);
                            ordenado=false;
                        }
                    }
                }
                i++;
            } while (ordenado==false && (i<vector.length-1));
            
            return vector;
        }


        /* 6.- Añadir a la clase un método construyeArray que reciba un número entero y devuelva un array de números
enteros con sus elementos inicializados a la posición de cada elemento (sólo que comenzando por 1 en lugar
de 0). Por ejemplo, si se le pasa como parámetro 5, devolvería el array de números enteros: {1,2,3,4,5}.

*/
        
        public static int[] construyeArray(int n) {
            int[] a = new int[n];
            for (int i=0;i<n;i++) {
                a[i] = i+1;
            }
            return a;
        }
        
        /* 15.- Lo mismo del ejercicio 6, sólo que cada posición contiene 
        la suma de las posiciones anteriores. Por
ejemplo si se la pasa como parámetro 5, devolvería el array {1,1,2,4,8}. 
       6.- Añadir a la clase un método construyeArray que reciba un número entero y devuelva un array de números
enteros con sus elementos inicializados a la posición de cada elemento (sólo que comenzando por 1 en lugar
de 0). Por ejemplo, si se le pasa como parámetro 5, devolvería el array de números enteros: {1,2,3,4,5}.
        */
        public static int[] construyeArray2(int n) {
            if (n<1) {
                return null;
            }
            
            int[] a = new int[n];
            a[0] = 1;
            for (int i=1;i<n;i++) {
                for (int j = 0; j <i;j++) {
                    a[i] += a[j];
                }
            }
            return a;
        }


        /*
       Se comienza escribiendo todos los enteros impares desde 3 hasta N; 
        a continuación se elimina cada tercer elemento después de 3, 
        cada quinto elemento después de 5, etc., 
        hasta que los múltiplos de todos los enteros impares 
        menores que raiz cuadrada de N hayan sido eliminados. 
        Los enteros que quedan constituyen la lista exacta de los números primos entre 3
y N. Añadir el método cribaEratostenes que devuelva una mtriz de booleanos que nos indica si el número de
la posición correpondiente del índice es primo (true). Por ejemplo una llamada al método
cribaEratostenes(13) debería devolver la matriz
{false,true,true,true,false,true,false,true,false,false,false,true,false,true}[0] [1] [2] [3] [4] [5] [6] [7] [8] [9] [10] [11] [12] [13]
        */
        
        
	public static boolean[] cribaEratostenes(int n) {
		boolean[] res = new boolean[n+1];
                res[1] = res[2] = true;
                // marcar todos los impares true (candidatos de ser primos)
		for (int i=3;i<=n;i+=2) {
			res[i]=true;
		}
	 	
		// busca indice de primer true y tomalo como multiplicador
		int multiplo = 3;
		do {
			while (res[multiplo] == false) multiplo++;
			for (int i=multiplo;i<=n;i+=multiplo) {
				res[i] = false;
			}			
			multiplo++;
		}
		while (multiplo<Math.sqrt(n));
				
		return res;
	}
	public static String printBool(boolean[] matriz) {
		String res="{";
		for (int i=0;i<matriz.length-1;i++) {
			res+="["+i+"]: ";
			if (matriz[i]) res+="true";
			else res+="false";
			res+=", \n";
		}
		res+="["+((matriz.length)-1)+"]: "+matriz[matriz.length-1]+"}";
		return res;
	}
        
        
        /* 7.- Añadir a la clase un método tabla Multiplicar 
            que devuelve un array con la tabla de multiplicar del
número que se le pase por parámetro.
    */
        public static int[] tablaMultiplicar(int n) {
            int[] res = new int[11];
            for (int i=0;i<11;i++) {
                res[i] = i*n;
            }
            return res;
        }
        
        /* 8.- Añadir a la clase un método maximo que reciba un array de enteros y nos devuelve el mayor. */
        public static int maximo(int[] a) {
            int max=a[0];
            for (int i=1;i<a.length;i++) {
                if (a[i]>max) max=a[i];
            }
            return max;
        }
        
        public static int[] minmax(int[] a) {
            int min=a[0];
            int max=a[0];
            for (int i=1;i<a.length;i++) {
                if (a[i]>max) max=a[i];
                if (a[i]<min) min=a[i];
            }
            int res[] = {min,max};
            return res;
        }
        
        /*  Método posicionArray que recibe un array de enteros y un número 
        entero y me devuelve la posición de éste último dentro del array.
        */
        
        public static int posicionArray(int[] a, int n) {
            int pos;
            int i=0;
            while ((a[i] != n) && (i<=n)) {
                i++;
            }
            if (i!=n+1) {
                pos=i;
            }
            else {
                pos=-1;
            }
            
            /*
            for (int i=0;i<a.length;i++) {
                if (a[i] == n) {
                    pos=i;
                    break;
                }
            }
            */
            
            return pos;
        }
        
        
        public static int posicionArray(int[] a, int n, int posIni) {
            if (posIni<0 || posIni>a.length-1) { return -1; }
            for (int i=posIni; i <a.length; i++) {
                if (a[i]==n) {
                    return i;
                }
            }
            return -1;
        }
        
        public static int[] posicionArray2(int[] a, int n) {
            /* recorrer dos veces. La primera vec para determinar la dimension 
            del array
            */
            int cont =0, pos, ind=0;
            for (int i=0;i<a.length;i++) {
                if (a[i]==n) cont++;
            }
            
            int[] res = new int[cont];
            
            for (pos=0;pos<a.length && pos!=-1;pos++) {
                pos = posicionArray(a,n,pos);
                if (pos>=0) {
                    res[ind++]=pos++;
                }
                
            }
           
            return res;
        }
        
        public static double mediaArray(int[] a) {
            if (a.length==0) {
                return -1;
            }
            double media = 0;
            for (int i=0;i<a.length;i++) {
                media += a[i];
            }
            media /= a.length;
            return media;
        }
        
        public static int[] invierteArray(int[] a) {
            int[] res = new int[a.length];
            for (int i=0;i<a.length;i++) {
                res[a.length-1-i] = a[i];
            }
            return res;
        }
        
        /*
        Añadir un método arrayAleatorio que devolverá un array de n números enteros aleatorios comprendidos
entre inf y sup (ambos inclusive) y que no tenga ningún elemento repetido. P.ej.: arrayAleatorio(6,1,49) -->
{1,7,6,5,49,39}
        */
        public static int[] arrayAleatorio(int n, int inf, int sup) {
            int[] aleatorio = new int[n];
            int i=0;
            do {
                int candidato = (int) (Math.random()*(sup+1-inf)+inf);
                // probar si ya existe
                boolean existe=false;
                for (int j=0;j<i;j++) {
                    if (aleatorio[j] == candidato) existe=true;
                }
                if (existe==false) {
                    aleatorio[i] = candidato;
                    i++;
                }
            }
            while (i<n);
            return aleatorio;
        }
        
/* 4.- Realizar un algoritmo que lea una matriz cuadrada de dimensión máxima N*N 
        y haga lo siguiente:
a) Imprimir la media de los elementos de la diagonal principal.
b) Imprimir la media de los elementos de la diagonal secundaria.
c) Imprimir la suma de los elementos por debajo de la diagonal principal y por 
        encima de ella.
*/

        public static void diagMatriz(double[][] matriz) {
            int dimension = matriz.length;
            double mediaPrincipal = 0.0;
            double mediaSecundaria =0.0;
            // suma de los elementos por debajo de la diagonal principal:
            // sumaSOE
            // suma de los elementos por encima de la diagonal principal:
            // sumaNE
            double sumaSO=0.0,sumaNE=0.0;
            
            for (int i=0;i<dimension;i++) {
                mediaPrincipal += matriz[i][i];
                mediaSecundaria += matriz[i][dimension-1-i];
                for (int j=i+1;j<dimension;j++) {
                    sumaSO+= matriz[j][i];
                    sumaNE+= matriz[i][j];
                }
            }
            mediaPrincipal /= dimension;
            mediaSecundaria /= dimension;
            System.out.printf("La media de los elementos de la diagonal principal es: %10.2f\n",mediaPrincipal);
            System.out.printf("La media de los elementos de la diagonal secundaria es: %10.2f\n",mediaSecundaria);
            System.out.printf("La suma de los elementos por abajo de la diagonal principal es: %10.2f\n",sumaSO);
            System.out.printf("La suma de los elementos por encima de la diagonal principal es: %10.2f\n",sumaNE);
        }
        
        
        
        
        public static String devuelveArray(int[] a) {
            String res="{";
            if (a==null) {
                res = res+"}";
            }
            else {
                int i;
                for (i=0;i<a.length-1;i++) {
                    res+=" ["+i+"]: ";
                    res+=a[i];
                    res+=", \n";
                }
                res+=" ["+i+"]: "+a[i]+"}";
            }
            return res;
        }
        public static String devuelveArray(double[] a) {
            String res="{";
            if (a==null) {
                res = res+"}";
            }
            else {
                int i;
                for (i=0;i<a.length-1;i++) {
                    res+=" ["+i+"]: ";
                    res+=a[i];
                    res+=", \n";
                }
                res+=" ["+i+"]: "+a[i]+"}";
            }
            return res;
        }
        
        public static String devuelveArray(int[][] a) {
            String res="";
            int i,j;
            for (i=0;i<a.length;i++) {
                res += " ["+a[i][0];

                for (j=1;j<a[0].length;j++) {
                    res+=" | "+a[i][j];
                }
                 res+="]\n";
            }
            return res;
        }        
        public static String devuelveArray(double[][] a) {
            String res="";
            int i,j;
            for (i=0;i<a.length;i++) {
                /* res += " ["+a[i][0]; */
                res += String.format(" [%5.2f",a[i][0]);

                for (j=1;j<a[0].length;j++) {
                    /* res+=" | "+a[i][j]; */
                    res += String.format(" | %5.2f ", a[i][j]);
                }
                 res+="]\n";
            }
            return res;
        }        
        
        public static String devuelveArray(boolean[] a) {
            String res="{";
            if (a==null) {
                res = res+"}";
            }
            else {
                int i;
                for (i=0;i<a.length-1;i++) {
                    res+=" ["+i+"]: ";
                    res+=a[i];
                    res+=", \n";
                }
                res+=" ["+i+"]: "+a[i]+"}";
            }
            return res;
        }
        
        
        public static int[] unirMatrices(int[] m1,int[] m2) {
            int dimRes = m1.length+m2.length;
            int[] res = new int[dimRes];
            
            // paso 1: copiar matrices m1,m2 en resultado res
            for (int i=0;i<m1.length;i++) {
                res[i] = m1[i];
            }
            for (int i=0;i<m2.length;i++) {
                res[m1.length+i] = m2[i];
            }
            
            // paso 2: ordenar matriz resultado
            // doble bucle, comparando posición i con todo a la derecha, y
            // intercambiar valores si necesario
            
            for (int i=0;i<res.length-1;i++) {
                for (int j=i+1;j <res.length;j++) {
                    if (res[j]<res[i]) {
                        int temp= res[i];
                        res[i]=res[j];
                        res[j]=temp;
                    }
                }
            }
            return res;
        }
        
        /*
        static boolean maximo(int[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    */
        /*
        Añadir un método eliminaRepetidos que reciba una matriz de enteros y devuelva una matriz con los
elementos repetidos elminados. P.ej.: eliminaRepetidos(new int[]{4,3,2,1,4,2,1,5,7,5}) devolvería:
{4,3,2,1,5,7}
        */
        
        public static int[] eliminaRepetidos(int[] a) {
            // crear una variable para la dimension del array que se devolverá
            int dimension=a.length;
            //  crear array booleano para marcar los elementos que se van a eliminar
            boolean[] dobles = new boolean[a.length];
            
            //recorrer el array de entrada y marcar los dobles
            for (int i=0;i<a.length-1;i++) {
                for (int j=i+1;j<a.length;j++) {
                    if (a[i]==a[j]) {
                        dobles[j] = true;
                        
                    }
                }
            }
            
            //calcular numero de "falsos"   
            for (int i=0;i<dobles.length;i++) {
                if (dobles[i]) dimension--;
            }
            
            int[] res= new int[dimension];
            // copiar los elementos únicos al nuevo array de dimensión reducida
            int j=0;
            for (int i=0;i<a.length;i++) {
                if (dobles[i]==false) {
                    res[j] = a[i];
                    j++;
                }
            }
            return res;
        }
	
	public static void main(String args[]) {
            boolean erat[] = cribaEratostenes(100);
            System.out.println(printBool(erat));	
	}
}
