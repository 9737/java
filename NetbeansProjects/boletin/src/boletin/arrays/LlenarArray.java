/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin.arrays;

import java.util.Scanner;
/**
 *
 * @author philipp
 */
public class LlenarArray {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce la cantidad de valores que cas a introducir: ");
        int n = teclado.nextInt();
        int valores[] = new int[n];
        for (int i=0;i<n;i++) {
            System.out.println("Introduce valor no "+i);
            valores[i] = teclado.nextInt();
        }
        System.out.println(boletin.arrays.UtilidadesMatrices.devuelveArray(valores));
        
    }
}
