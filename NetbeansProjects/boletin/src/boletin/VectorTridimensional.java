/*
 14. Realizad una clase VectorTridimensional que implemente la 
suma, resta, producto escalar, producto vectorial y módulo del vector, 
y un método numVectores que nos devuelva el número de
objetos VectorTridimensional creados en nuestra aplicación.
 */
package boletin;

/**
 *
 * @author philipp
 */
public class VectorTridimensional {
    private static int vectoresCreados;
    private double x,y,z;
    
    public VectorTridimensional() {
        
    }
    public VectorTridimensional(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        vectoresCreados++;
    }
    public static int numVectores() {
        return vectoresCreados;
    }
    public VectorTridimensional suma(VectorTridimensional v2) {
        return new VectorTridimensional(this.x + v2.x,
                                        this.y + v2.y,
                                        this.z + v2.z);
    }
    public VectorTridimensional resta(VectorTridimensional v2) {
        return new VectorTridimensional(this.x - v2.x,
                                        this.y - v2.y,
                                        this.z - v2.z);
    }
    public VectorTridimensional prodEscalar(double p) {
        return new VectorTridimensional(this.x * p,
                                        this.y * p,
                                        this.z * p);
    }
    public VectorTridimensional prodVectorial(VectorTridimensional v2) {
        /*     {\mathbf u}=u_{x}{\mathbf i}+u_{y}{\mathbf j}+u_{z}{\mathbf k}
    v = v x i + v y j + v z k {\displaystyle \mathbf {v} =v_{x}\mathbf {i} +v_{y}\mathbf {j} +v_{z}\mathbf {k} } {\mathbf v}=v_{x}{\mathbf i}+v_{y}{\mathbf j}+v_{z}{\mathbf k}
    w = w x i + w y j + w z k {\displaystyle \mathbf {w} =w_{x}\mathbf {i} +w_{y}\mathbf {j} +w_{z}\mathbf {k} } {\mathbf w}=w_{x}{\mathbf i}+w_{y}{\mathbf j}+w_{z}{\mathbf k}

        w_x = u_y * v_z - u_z * v_y
        w_y = u_z * v_x - u_x * v_z
        w_z = u_x * v_y - u_y * v_x
                
        donde   Vector1 = u_x, u_y, u_z
                Vector2 = v_x, v_y, v_z
                Vector3 = w_x, w_y, w_z
        
        */
        double x,y,z;
        x = this.y * v2.z - this.z * v2.y;
        y = this.z * v2.x - this.x * v2.z;
        z = this.x * v2.y - this.y * v2.x;
        return new VectorTridimensional(x,y,z);
    }
    
    @Override
    public String toString() {
        return "["+this.x+"|"+this.y+"|"+this.z+"]";
    }
    
    public double modulo() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }
}