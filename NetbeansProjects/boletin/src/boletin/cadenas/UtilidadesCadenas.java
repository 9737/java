/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin.cadenas;

/**
 *
 * @author daw
 */
public class UtilidadesCadenas {
    
    public static int cuentaRepeticiones(char c, String s) {
        int contador =0;
        for (int i=0;i<s.length();i++) {
            if (s.charAt(i) == c) {
                contador++;
            }
        }
        return contador;
    }
    
    public static void muestraAscii() {
        String res="";
        char c,C,n;
        for (int i='a';i<='z';i++) {
            res += (char) i;
        }
        for (int i='A';i<='Z';i++) {
            res += (char) i;
        }
        for (int i='0';i<='9';i++) {
            res += (char) i;
        }
        
        // alternativamente mostrar 3 columnas  en un búcle
        // quitando n cuando salga de '9'
        
        System.out.println(res);
    }
    
    public static String toUpperMi(String s) {
        
        String res="";
        
        int[] especiales = new int[]{'á','é','í','ó','ú','ü','ñ'};
        
        // recorrer String (con charAt)
        
        for (int i =0;i<s.length();i++) {
            // si es una letra y es minúscula
            char c=s.charAt(i);
            if ((c >='a') && (c <='z') || boletin.arrays.UtilidadesMatrices.posicionArray((int[])especiales, c)>-1)
                c-=32;
            res = res + c;
        }
        
        
        return res;
    }
    
    
    public static String toLowerMi(String s) {
        String res="";
        
        int[] especiales = new int[]{'á','é','í','ó','ú','ü','ñ'};
        
        // recorrer String (con charAt)
        
        for (int i =0;i<s.length();i++) {
            // si es una letra y es mayúscula
            char c=s.charAt(i);
            if ((s.charAt(i) >='A') && (s.charAt(i) <='Z') || boletin.arrays.UtilidadesMatrices.posicionArray((int[])especiales, c)>-1) 
                c+=32;
            res = res + c;
        }
        
        return res;
        
    }
    
    /* Método toUpper*/
    public static String convierteCadena(String s, boolean mayus) {
        String res=""; char c,inf,sup; int especiales[], inc;
        if (mayus) {
            especiales=new int[]{'á','é','í','ó','ú','ñ','ü'};
            inf='a'; sup='z'; inc='A'-'a';
        } else {
            especiales=new int[]{'Á','É','Í','Ó','Ú','Ñ','Ü'};
            inf='A'; sup='Z'; inc='a'-'A';
        }
        for(int i=0;i<s.length();i++) {
            c=s.charAt(i);
            if (inf <= c && c <= sup || boletin.arrays.UtilidadesMatrices.posicionArray(especiales,c)>=0 ) {
                c+=inc;
            }
            res+=c;
        }
        return res;
    }
    public static String toUpper(String s) {
        return convierteCadena(s,true);
    }
    public static String toLower(String s) {
        return convierteCadena(s,false);
    }
    
    
    public static int cuentaRepeticiones(String needle, String haystack) {
        int contador=0;
        int i, j; 
        boolean encontrado;
        for (i=0;i<haystack.length()-needle.length();) {
            
            if (haystack.charAt(i)== needle.charAt(0)) {
                // boolean existe = true;
                for (j=1;j<=needle.length();j++) {
                    if(needle.charAt(j) != haystack.charAt(i+j)) {
                        break;
                    }
                }
                if (j==needle.length()) {
                    //no encontrado
                    contador++;
                    i+=needle.length();
                }
                else {
                    i++;
                }
                /*
                if (existe) {
                    contador++;
                    j+=needle.length(); 
                }
                */
            }
            else {
                i++;
            }
        }
        return contador;
    }
    
    
}

