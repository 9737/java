
package boletin.cadenas;

/**
 *
 * @author philipp
 */



public class Palindromo {
    
    public static void infoSalida() {
        System.out.println("Programa que reciba una frase y diga si es "+
                "palíndromo (que coincida la letre de la derecha y dela "+
                "izquierda). Por ejemplo 'reconocer'");
        System.out.println("Uso: java Palindromo {Frase a comprobar}");
        System.exit(-1);
    }
    
    public static void main(String args[]) {
        
        if (args.length==0) {
            infoSalida();
        }
        
        //concatenar todos las palabras en un string
       
        String entrada="";
        for (int i=0;i<args.length;i++) {
            entrada = entrada+args[i];
        }
        // quitar todos espacios blancos
        entrada = entrada.replaceAll("\\s","");
        // poner todo en minúscula
        entrada = entrada.toLowerCase();
        
        //convert String to CharArray
        char matriz[] = entrada.toCharArray();
        
        boolean palindromo = true;
        for (int i=0;i<matriz.length;i++) {
            if (matriz[i] != matriz[matriz.length-1-i]) {
                palindromo = false;
            }
        }
        for (int i=0; i<args.length; i++) {
                System.out.print(args[i]+" ");
            }
        if (palindromo) {
            
            System.out.println(" es un palíndromo");
        }
        else {
            System.out.println(" no es un palíndromo");
        }
    }
}
