/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boletin;
    
import static boletin.arrays.UtilidadesMatrices.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

    
    
/**
 *
 * @author philipp
 */
public class NotasAlumnos {
    private double[] nota;
    
    public NotasAlumnos(String nombreEntrada) throws FileNotFoundException {
        leerDatos(nombreEntrada);
    }
     
    public void leerDatos(String nombreEntrada) throws FileNotFoundException {
        // leer datos desde fichero

        FileInputStream fis= new FileInputStream(nombreEntrada);
                                                                                           System.setIn(fis);

        int numAlumnos;

        Scanner teclado = new Scanner(System.in);

        System.out.println("Cuantos alumnos hay: ");
        numAlumnos = teclado.nextInt();
        nota = new double[numAlumnos];
        
        for (int i =0;i<numAlumnos; i++) {
            nota[i] = teclado.nextDouble();
        }
    }

    public void ordenarNotas() {
        burbuja(nota,false);
        System.out.println(devuelveArray(nota));
    }
    
    public void imprimeNotas() {
        double media = (nota[0]+nota[1]+nota[2]) / 3.0;
        System.out.printf("Media de los 3 mejores: %.2f", media);
    }
    
    public static void main(String[] args) throws FileNotFoundException{
        NotasAlumnos n1 = new NotasAlumnos("/tmp/datos_alumnos.txt");
        n1.ordenarNotas();
        n1.imprimeNotas();
    }
    
}
