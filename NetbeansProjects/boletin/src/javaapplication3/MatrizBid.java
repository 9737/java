/*
 Implementar una clase  matrizBid  que represente una matriz 
bidimensional y que implemente las operaciones de suma, resta y 
producto
 */
package javaapplication3;

public class MatrizBid {
    private double[][] x;
    public double[][] getX() {
        return this.x;
    }
    public MatrizBid() {
    }
    public MatrizBid(double x[][]) {
        int fils = x.length;
        int cols = x[0].length;
        this.x = new double[fils][cols];
        for (int fila=0; fila<x.length; fila++) {
            for (int col=0; col<x[0].length; col++) {
                this.x[fila][col] = x[fila][col];
            }
        }
    }
    
    public String toString() {
        String res = "";
        int fils = this.x.length;
        int cols = this.x[0].length;
        for (int fila=0; fila<this.x.length; fila++) {
            for (int col=0; col<this.x[0].length; col++) {
                res+=this.x[fila][col]+" ";
            }
            res += "\n";
        }
        return res;
    }
    public MatrizBid(MatrizBid m2) {
        this.x = new double[m2.getX().length][m2.getX()[0].length];
        this.x = m2.getX();
    }
    
    public MatrizBid suma(MatrizBid m2) {
        if ((this.x.length!=m2.getX().length) || 
            (this.x[0].length != m2.getX()[0].length)) {
            System.out.println("No puedo sumar matrices de distinto tamaño");
            return null;
        }
        
        int fils=x.length, cols = x[0].length;
        double y[][] = new double[fils][cols];
        for (int fila=0;fila<fils; fila++) {
            for (int col=0; col<cols;col++) {
                y[fila][col] = this.x[fila][col] + m2.getX()[fila][col];
            }
        }
        return new MatrizBid(y);
    }
    
    public MatrizBid resta(MatrizBid m2) {
        if ((this.x.length!=m2.getX().length) || 
            (this.x[0].length != m2.getX()[0].length)) {
            System.out.println("No puedo restar matrices de distinto tamaño");
            return null;
        }
        
        int fils=x.length, cols = x[0].length;
        double y[][] = new double[fils][cols];
        for (int fila=0;fila<fils; fila++) {
            for (int col=0; col<cols;col++) {
                y[fila][col] = this.x[fila][col] - m2.getX()[fila][col];
            }
        }
        return new MatrizBid(y);
    }
    
    public MatrizBid prodVector(MatrizBid m2) {
        if ((this.x.length!=m2.getX()[0].length) || 
            (this.x[0].length != m2.getX().length)) {
            System.out.println("No puedo sumar matrices de distinto tamaño");
            return null;
        }
        int fils=x.length, cols = x[0].length;
        double x[][] = this.getX();
        double y[][] = m2.getX();
        double z[][] = new double[fils][cols];
        
        for (int filz=0;filz<fils;filz++) {
            for (int colz=0; colz<cols;colz++) {
                // multiplicar fila de x con columna de y
                double suma = 0;
                for (int i=0; i < cols; i++) {
                    suma += x[filz][i] * y[i][colz];
                }
                z[filz][colz] = suma;
            }
        }
        return new MatrizBid(z);
    }
}
