import java.util.Scanner;
public class Billetes {
	
	public static void main(String[] args) {
		// billetes:
		// 200,100,50,20,10,5

		System.out.print("Introduce el importe en Euros: ");
		Scanner s = new Scanner(System.in);
		int importe = s.nextInt();
		int temp = importe;
		int bill500 = temp / 500;
		temp = temp % 500;
		int bill200 = temp / 200;
		temp = temp % 200;
		int bill100 = temp / 100;
		temp = temp % 100;
		int bill50 = temp / 50;
		temp = temp % 50;
		int bill20 = temp / 20;
		temp = temp % 20;
		int bill10 = temp / 10;
		temp = temp % 10;
		int bill5 = temp / 5;
		temp = temp % 5;

		System.out.println("Importa total en Euros: " + importe);
		System.out.println("Cambio optimo: ");
		System.out.println(bill500 + " billetes de 500 Euros");
		System.out.println(bill200 + " billetes de 200 Euros");
		System.out.println(bill100 + " billetes de 100 Euros");
		System.out.println(bill50 + " billetes de 50 Euros");
		System.out.println(bill20 + " billetes de 20 Euros");
		System.out.println(bill10 + " billetes de 10 Euros");
		System.out.println(bill5 + " billetes de 5 Euros");
		System.out.println(temp + " Euros en cambio");
	}
}