public class Letra {
	public static char genAleatorio() {
		return (char) (Math.random()*26+65);
	}

public static char genAleatorio(int inf, int sup) {

		return (char) (Math.random()*(sup-inf)+inf);
	}

	public static void main(String[] args) {
		char a = genAleatorio(65,91);
		char b = genAleatorio(65,91);
		char c = genAleatorio(65,91);
		char d = genAleatorio(65,91);
		char e = genAleatorio(65,91);
		char f = genAleatorio(65,91);

		while (a==b) b = genAleatorio(65,91);
		while ((a==c) || (b==c)) c = genAleatorio(65,91);
		while ((a==d) || (b==d) || (c==d) ) d = genAleatorio(65,91);
		while ((a==e) || (b==e) || (c==e) || (d==e) ) e = genAleatorio(65,91);
		while ((a==f) || (b==f) || (c==f) || (d==f) || (e==f) ) f = genAleatorio(65,91);

		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);

		//ordenar ascendiente bubble sort
		char temp;
		if (a>b) {temp=b; b=a; a=temp;}
		
		if (a>c) {temp=c; c=a; a=temp;}
		if (b>c) {temp=c; c=b; b=temp;}

		if (a>d) {temp=d; d=a; a=temp;}
		if (b>d) {temp=d; d=b; b=temp;}
		if (c>d) {temp=d; d=c; c=temp;}

		if (a>e) {temp=e; e=a; a=temp;}
		if (b>e) {temp=e; e=b; b=temp;}
		if (c>e) {temp=e; e=c; c=temp;}
		if (d>e) {temp=e; e=d; d=temp;}

		if (a>f) {temp=f; f=a; a=temp;}
		if (b>f) {temp=f; f=b; b=temp;}
		if (c>f) {temp=f; f=c; c=temp;}
		if (d>f) {temp=f; f=d; d=temp;}		
		if (e>f) {temp=f; f=e; e=temp;}	

		System.out.println(a + " " + b + " " + c + " " + d + " " + e + " " + f);

	}
}