import java.util.Scanner; // Importo la API/librería Scanner

public class Valoresteclado {
    public static void main(String args []){

        // Creo un objeto de tipo Scanner, con la variable "teclado"
        // Para iniciarlo, le decimos que va a ser un nuevo objeto de tipo Scanner
        // Le decimos que introduciremos datos con (System.in)
        Scanner teclado = new Scanner (System.in);

        double n1, n2, resultado; // Declaro las variables

        System.out.println("Escribe el primer numero:"); // Imprimo por pantalla que escriba el número
        n1 = teclado.nextDouble(); // Le asigno a n1 lo que el usuario ha escrito

        System.out.println("Escribe el segundo numero:");
        n2 = teclado.nextDouble();

        resultado = n1 + n2; // Recojo los dos datos en una sola variable para imprimirla por pantalla en el mensaje final

        System.out.println("El resultado es " + resultado);
    }
}

