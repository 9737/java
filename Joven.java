public class Joven {
	byte edad;
	byte nivelDeEstudios;
	double ingresos;
	String nombre;
	String apellido1;
	String apellido2;
	Boolean jasp;

	public Joven(String nombre, String apellido1, String apellido2, byte edad,
			 byte nivelDeEstudios, double ingresos) {
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.edad = edad;
		this.nivelDeEstudios = nivelDeEstudios;
		this.ingresos = ingresos;
	}

	public void establecerJasp() {
	 jasp = ((edad < 28) && (nivelDeEstudios > 3) && (ingresos>28000));

	}

	public static void main(String args[]) {
		Joven p = new Joven("Pepito", "López", "Pérez", (byte) 18,(byte)3,1758.65);
		Joven j = new Joven("Luisa", "López", "Martinez", (byte) 15,(byte)2,0.0);
		p.establecerJasp();
		j.establecerJasp();
		System.out.println(p.jasp);
		System.out.println(j.jasp);

	}
}