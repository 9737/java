public class Cuenta {
	protected double saldo;
	public Cuenta() {
	}
	public Cuenta(double saldo) {
		this.saldo = saldo;
	}

	public void abonar(double cantidad) {
		this.saldo += cantidad;
	}

	public void retirar(double cantidad) {
		this.saldo -= cantidad;
	}
	public double getSaldo() {
		return saldo;
	}
}