public class Test {
	public static void main(String args[]) {
		Cuenta c1 = new CuentaCheques(2000.00);
		Cuenta c2 = new CuentaAhorro(500.00);
		c2.retirar(50.0);
		c1.abonar(50.0);
		
		System.out.println("Saldo de cuenta cheques "+c1.getSaldo());
		System.out.println("Saldo de cuenta ahorro "+c2.calcularSaldo(0.002));
	}
}