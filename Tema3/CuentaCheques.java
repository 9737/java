public class CuentaCheques extends Cuenta {
	private int movimientos;

	public int movimientosDiarios() {
		return movimientos;
	}
	public void abonar(double cantidad) {
		super.abonar(cantidad);
		movimientos++;
	}
	public void retirar(double cantidad) {
		super.retirar(cantidad);
		movimientos++;
	}
}