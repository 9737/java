/* Enunciado
- Programa que reciba un número entre [1,7] pasado desde línea de comandos y diga el día de la semana. ( p.ej. 1 -> Lunes)

*/

public class NombreDia
{
    public static void main(String args[])
    {
        if (args.length == 0)
        {
            System.out.println("Uso: $ java NombreDia {número}");
            System.exit(0);
        }

        int ordinal = 0;
        try
        {
            ordinal = Integer.parseInt(args[0]);
        }
        catch (NumberFormatException e)
        {
            System.out.println("Uso: $ java NombreDia {número}");
            System.exit(0);
        }
        switch (ordinal)
        {
        case 1:
            System.out.println("Lunes");
            break;
        case 2:
            System.out.println("Martes");
            break;
        case 3:
            System.out.println("Miércoles");
            break;
        case 4:
            System.out.println("Jueves");
            break;
        case 5:
            System.out.println("Viernes");
            break;
        case 6:
            System.out.println("Sábado");
            break;
        case 7:
            System.out.println("Domingo");
            break;
        default:
            System.out.println("La semana solamente tiene 7 días.");
        }
    }
}
