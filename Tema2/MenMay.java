public class MenMay {
/* 1.- Programa que reciba tres números y diga cuál es el menor y el mayor
*/
    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Uso: $ java MayMen {n1} {n2} {n3}");
            System.exit(1);
        }
        int n1=Integer.parseInt(args[0]);
        int n2=Integer.parseInt(args[1]);
        int n3=Integer.parseInt(args[2]);

        // ordenar números utilizando ordenamiento de burbuja
        int temp;
        if (n1>n2) {
            temp = n2;
            n2=n1;
            n1=temp;
        }
        if (n2>n3) {
            temp=n3;
            n3=n2;
            n2=temp;
        }
        if (n1>n2) {
            temp = n2;
            n2=n1;
            n1=temp;
        }

        System.out.println("El menor número es "+ n1);
        System.out.println("El mayor número es "+ n3);
    }
}
