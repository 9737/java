public class Mult5 {
/*
- Programa que reciba un número pasado desde línea de comandos 
	y muestre todos los números múltiplos de 5 comprendidos 
	entre 0 y ese número
*/
  public static void main(String[] args) {
  	int limite = Integer.parseInt(args[0]);
  	int i=5;

  	while (i<limite) {
  		if (i%5==0) {
  			System.out.println(i+" es un múltiple de 5");
  		}
  		i+=5;
  	}

  }

}