public class Prueba2 {
	public static void main(String args[]) {

		String nota = "";
		
		if (args.length<1) {
			System.out.println("Necesito una valoración válida.");
			System.exit(1);
		}

		for (String i : args) {
			nota = nota.concat(i+" ");
		}
		System.out.println("Argumentos: " + nota);
		nota = nota.toLowerCase();
		nota = nota.trim();

		/*
		if (nota==10) {
			System.out.println("Matrícula de Honor");
		} else if (nota==9) {
			System.out.println("Sobresaliente");
		} else if (nota==8 || nota==7) {
			System.out.println("Notable");
		} else if (nota==6) {
			System.out.println("Bien");
		} else if (nota==5) {
			System.out.println("Suficiente");
		} else {
			System.out.println("Suspenso");
		}
		*/


		switch(nota) {
			case "matrícula": System.out.println("10"); break;
			case  "sobresaliente": System.out.println("9"); break;
			case  "notable alto": System.out.println("8"); break; 
			case  "notable bajo": System.out.println("7"); break;
			case  "bien": System.out.println("6"); break;
			case  "suficiente": System.out.println("5"); break;
			case  "suspenso": System.out.println("< 5"); break;
			case "notable": System.out.println("Especifica si es alto o bajo"); break;
			default: System.out.println("No has entrado una nota válida");
		}
		//terminación del break

		// opción concatenar todos los args

	}

}