public class Ecuacion {
/*
3.- Programa que reciba tres números a, b y c, 
coeficientes de una ecuación de segundo grado 
y dé sus soluciones (usar try catch)
f(x)=ax2+bx+c con a ≠ 0 (aquí permitimos a=0, aunque ya no sea una ecuación 
de segundo grado)
formula para las dos resoluciones:
x = (-b +- sqrt(b*b-4*a*c))/2*a
*/
    public static void main(String args[]) {
        if (args.length != 3) {
            System.out.println("Programa que reciba tres numeros a,b y c,");
            System.out.println("coeficientes de una ecuación de segundo grado");
            System.out.println("y dé soluciones.");
            System.out.println("Uso: java Ecuacion {a} {b} {c}");
            System.exit(1);
        }
        double a,b,c,x1,x2;
        a = Double.parseDouble(args[0]);
        b = Double.parseDouble(args[1]);
        c = Double.parseDouble(args[2]);
        x1 = 0.0;
        x2 = 0.0;

        if (b*b-4*a*c<0) {
            System.out.println("La raíz cuadrada solamente es definido para números reales no negativos.");
            System.exit(1);
        }

        try {
            x1 = (-1.0*b+Math.sqrt(b*b-4*a*c))/(2*a);
            x2 = (-1.0*b-Math.sqrt(b*b-4*a*c))/(2*a);
        }
        catch (NumberFormatException e)
        {
            System.out.println("Se ha producido una excepción.");
            e.printStackTrace();
        }
        System.out.println("               2         1/2    ");
        System.out.println("        -b + (b  - 4ac )        "); 
        System.out.println(" x    = ____________________    "); 
        System.out.println("  1                             "); 
        System.out.println("                2a              "); 
        System.out.println();
        System.out.println("               2         1/2    "); 
        System.out.println("        -b - (b  - 4ac )        "); 
        System.out.println(" x    = ____________________    "); 
        System.out.println("  2                             "); 
        System.out.println("                2a              ");
        System.out.println();
        System.out.println("Coeficientes introducidos:");
        System.out.println("a = "+a);
        System.out.println("b = "+b);
        System.out.println("c = "+c);
        System.out.println();
        System.out.println("x = "+x1);
        System.out.println(" 1");
        System.out.println();
        System.out.println("x = "+x2);
        System.out.println(" 2");
    }  
}
