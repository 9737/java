/* Enunciado
- Programa quwe reciba un número entre [1,12] pasado desde linea de comandos y diga el mes correspondiente
p.ej.6-> Junio)

*/

public class NombreMes
{
    public static void main(String args[])
    {
        if (args.length == 0)
        {
            System.out.println("Uso: $ java NombreMes {número}");
            System.exit(0);
        }

        int ordinal = 0;
        try
        {
            ordinal = Integer.parseInt(args[0]);
        }
        catch (NumberFormatException e)
        {
            System.out.println("Uso: $ java NombreMes {número}");
            System.exit(0);
        }
        switch (ordinal)
        {
        case 1:
            System.out.println("Enero");
            break;
        case 2:
            System.out.println("Febrero");
            break;
        case 3:
            System.out.println("Marzo");
            break;
        case 4:
            System.out.println("Abril");
            break;
        case 5:
            System.out.println("Mayo");
            break;
        case 6:
            System.out.println("Junio");
            break;
        case 7:
            System.out.println("Julio");
            break;
        case 8:
            System.out.println("Agosto");
            break;
        case 9:
            System.out.println("Septiembre");
            break;
        case 10:
            System.out.println("Octubre");
            break;
        case 11:
            System.out.println("Noviembre");
            break;
        case 12:
            System.out.println("Diciembre");
            break;
        default:
            System.out.println("El año solamente tiene 12 meses.");
        }
    }
}
