public class TodoPar
/* - Programa que reciba un número pasado desde línea de comandos e imprima todos los números pares comprendidos entre 0 y ese número

*/
{
    public static void main(String[] args)
    {
        int numero = 0;
        numero = Integer.parseInt(args[0]);
        int i = 0;
        while (i < numero)
        {
            System.out.println(i);
            i += 2;
        }
    }
}