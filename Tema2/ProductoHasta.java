public class ProductoHasta
{
    /* - Programa que reciba un número pasado desde línea de comandos 
    	e imprima el producto de todos los números comprendidos entre 1 
    	y ese número

    	- alternativa:
            la facultad de n
    */
    public static void main(String[] args)
    {
        int numero = 0;
        numero = Integer.parseInt(args[0]);
        int suma = 0;
        int i = 0;
        while(++i <= numero)
        {
            suma += i;
        }
        System.out.println("La suma de todos numeros hasta " + numero + " es " + suma + ".");

        double gauss = numero / 2.0 *(1+numero);
        System.out.println("Y con formula gauss sale " + gauss);
    }

}