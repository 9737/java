/* - Programa que reciba un número pasado desde línea de comandos 
	e imprima su tabla de multiplicar
*/

public class Tabla {
	public static void main(String[] args) {
		int numero =0;
		if (args.length == 0) {
			System.err.println("Uso: $ java Tabla {numero}");
			System.exit(1);
		}
		try {
			numero = Integer.parseInt(args[0]);
		}
		catch(NumberFormatException e) {
			System.err.println("Uso: $ java Tabla {numero}");
			System.exit(1);
		}
		int i = 1;
		while (i<=10) {
			System.out.println(i+" * "+numero+" = "+i*numero);
			i++;
		}
	}
}