public class AleaPar {
/* 3.- Programa que genere un número aleatorio entre [100,200] 
e imprima el número y diga si es par o impar
*/
  public static void main(String[] args) {
  	int miran = (int) (100+Math.random()*101);
  	System.out.print("Hemos generado el número "+ miran);
  	if (miran%2==0) {
  		System.out.println(" y este número es par.");
  	}
  	else {
  		System.out.println(" y este número es impar.");
  	}

  }

}
