public class Prueba {
	public static void main(String args[]) {
		int nota;
		if (args.length<1) {
			System.out.println("Necesito un parámetro que sea una nota en el rango [1,10]");
			System.exit(1);
		}
		nota=Integer.parseInt(args[0]);
		if (nota<0 || nota>10) {
			System.out.println("Necesito una nota en el rango [1,10]");
			System.exit(2);

		}
		/*
		if (nota==10) {
			System.out.println("Matrícula de Honor");
		} else if (nota==9) {
			System.out.println("Sobresaliente");
		} else if (nota==8 || nota==7) {
			System.out.println("Notable");
		} else if (nota==6) {
			System.out.println("Bien");
		} else if (nota==5) {
			System.out.println("Suficiente");
		} else {
			System.out.println("Suspenso");
		}
		*/
		switch(nota) {
			case 10: System.out.println("Matrícula"); break;
			case  9: System.out.println("Sobresaliente"); break;
			case  8: 
			case  7: System.out.println("Notable"); break;
			case  6: System.out.println("Bien"); break;
			case  5: System.out.println("Suficiente"); break;
			default: System.out.println("Suspenso"); 
		}
		//terminación del break

	}

}