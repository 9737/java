import java.nio.Charset.*;
public class Ejercicio21 {
	public static void infoSalida() {
		System.out.println("** 21.- Programa que reciba un"+ 
			"número entero y lo imprima al revés. P.ej.: si "+ 
			"recibe el número 1759 deberéa imprimir 9571.");
		System.out.println("Uso: java Ejercicio21 {número}");
		System.exit(1);
	}
	public static void main(String[] args) {
		int n = 0, salida;
		if (args.length != 1) {
			infoSalida();
		}
		try {
			n = Integer.parseInt(args[0]);
		}
		catch (NumberFormatException ex) {
			System.out.println("Número no tiene el formato correcto.");
			ex.getMessage();
		}
		boolean negativo=false;
		if (n<0) {
			n = -n;
			negativo = true;
		}

		System.out.print("El número "+n);
		salida = 0;
		// leer dígito menos significante
		do {
			salida = salida * 10 + (n%10);
			n /= 10; //mover número un dígito a la derecha
		} while (n>0); // termina después del último dígito
		
		if (negativo) { salida = -salida; }
		System.out.println(" corresponde a "+ salida+ " cuando invertido.");
	}
}