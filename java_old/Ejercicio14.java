import java.util.Scanner;
public class Ejercicio14
{
/* Programa que genere un número aleatorio entre [1,100] y le dé al usuario la oportunidad de adivinarlo en 7 intentos como máximo...
 */
	static int MAXINTENT=7;
    public static void infoSalida()
    {
    }

    public static void main(String args[])
    {
		// crear número aleatorio
		int miNumero = (int) (Math.random()*100+1);
		Scanner teclado = new Scanner(System.in);
		int intentos = MAXINTENT;
		int tuNumero = 0;
		boolean ganado = false;
		while ((intentos>0) && (ganado==false)) {
			System.out.println("Te quedan "+intentos+" intentos");
//			System.err.println("Psst... el número es "+miNumero);
			System.out.print("Adivina mi número entre 1 y 100: ");
			tuNumero = teclado.nextInt();

			if (tuNumero == miNumero) {
				ganado = true;
			}
			intentos--;
		}
		if (ganado) {
			System.out.println("Bravo!: Has acertado mi número en "+
							(MAXINTENT-intentos)+" intentos!");
		}
		else {
			System.out.println("Ohh... Bueno, menos mal que no has apuesto dinero... Adios. A mi me gusta el número "+miNumero);
		}
    }
}
