public class Ejercicio20 {
    public static void infoSalida() {
        System.out.println("Programa que reciba un día, mes y año y diga si "
                + "la fecha es correcta (hay que tener en cuenta "
                + "si el año es bisiesto)");
        System.out.println("Uso: java Ejercicio20 {día} {mes} "
                + "{año}");
        System.exit(1);
    }

    public static void main(String args[]) {
        int dia = 1, mes = 1, año = 2000;
        boolean bisiesto = false;
        boolean fechaValida = true;
        if (args.length != 3) {
            infoSalida();
        }
        try {
            dia = Integer.parseInt(args[0]);
            mes = Integer.parseInt(args[1]);
            año = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            e.getMessage();
            infoSalida();
        }
        if ((dia < 1) || (dia > 31)) {
            System.out.println("El día es incorrecto.");
            infoSalida();
        }
        if ((mes < 1) || (mes > 12)) {
            System.out.println("El mes es incorrecto.");
            infoSalida();
        }
        if (año < 1582) {
            System.out.println("El año es incorrecto.");
			System.out.println("El calendario gregoriano fue introducido en 1582");
            infoSalida();
        }

        // año es bisiesto si es divisible por 4 pero no por 100
        if ((año % 4 == 0) && (año % 100 != 0)) {
            bisiesto = true;
        }
		// año si es bisiest si es divisible por 400
		if (año % 400 == 0) {
				bisiesto = true;
		}

        switch (mes) {
            case 2:
                if (dia > 29) {
                    fechaValida = false;
                } else if ((dia == 29) && !bisiesto) {
                    fechaValida = false;
                }
                break;
            case 4:
                if (dia == 31) {
                    fechaValida = false;
                }
                break;
            case 6:
                if (dia == 31) {
                    fechaValida = false;
                }
                break;
            case 9:
                if (dia == 31) {
                    fechaValida = false;
                }
                break;
            case 11:
                if (dia == 31) {
                    fechaValida = false;
                }
                break;
        }
        if (fechaValida) {
            System.out.println("La fecha " + año + "-" + mes + "-" + dia + 
                    " es una fecha válida.");
        } else {
            System.out.println("Esta fecha no es válida.");
        }
    }
}
