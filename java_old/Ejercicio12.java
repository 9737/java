public class Ejercicio12
{
    public static void infoSalida()
    {
        System.out.println("Programa que reciba un número de 3 "+
                        "cifras y diga si es un número de Armstrong");
        System.out.println("Uso: java Ejercicio12 n");
        System.exit(1);
    }
    public static void main(String args[])
    {
        if (args.length != 1)
        {
            infoSalida();
        }
        int n = 1;
        try
        {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e)
        {
            infoSalida();
        }
        if ((n < 100) || (n>999))
        {
            infoSalida();
        }

        int temp=n, sum=0, r;    
        while(n>0)
        {    
            r=n%10;    
            sum=sum+(r*r*r);    
            n=n/10;    
        }    
        if(temp==sum) {
            System.out.println(temp+" es un número armstrong");    
	    int centenas,decenas, unidades;
	    centenas = temp/100;
	    temp %= 100;
	    decenas = temp/10;
	    temp %= 10;
	    unidades = temp;
	    System.out.println(" = ("+centenas+"*"+centenas+"*"+centenas+") + "+
			    "("+decenas+"*"+decenas+"*"+decenas+") + "+
 				"("+unidades+"*"+unidades+"*"+unidades+") ");
       }
        else {
            System.out.println(temp+" no es número armstrong");              
        }
        
    }
}
