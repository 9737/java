import java.util.Scanner;
public class Ejercicio15 {
    public static void infoSalida() {
        System.out.println("Programa que imprima el \"lucky number\" de "+
			"una persona. Éste se consigue reduciendo la fecha de nacimiento "+
		   "a un sólo dígito. P.Ej. si la fecha de nacimiento es 11-02-1973, "+
		  "entonces el sería: 11+2+1973=1986 -> 1+9+8+6=24 -> 2+4=6. "+
		  "Entonces 6 sería el resultado. ");
        System.out.println("Uso: java Ejercicio15");
        System.exit(1);
    }
	public static int traeNumero(Scanner teclado) {
		int n = 0;
		try {
			n = teclado.nextInt();
		}
		catch ( Exception e) {
			infoSalida();
		}
		return n;
	}
    public static void main(String args[]) {

/*
        if (args.length!=1) {
            infoSalida();
        }
        int n=1;
        try {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e) {
            infoSalida();
        }
        if (n<0) {
            infoSalida();
        }
*/
        Scanner teclado = new Scanner(System.in);
        System.out.print("¿En que año naciste (4 digitos)?: ");
		int anio= traeNumero(teclado);
		System.out.println();
		System.out.print("¿En qué mes naciste (2 dígitos)?: ");
		int mes = traeNumero(teclado);
		System.out.println();
		System.out.print("¿Qué día naciste (2 dígitos)?: ");
		int dia = traeNumero(teclado);
		System.out.println();

		int luckynum = anio+mes+dia;
		int miles = luckynum/1000;
		luckynum %= 1000;
		int centenas = luckynum / 100;
		luckynum %= 100;
		int decenas = luckynum / 10;
		luckynum %= 10;
		int unidades = luckynum;

		// aquí machacamos la variable
		luckynum = miles+centenas+decenas+unidades;

		//repitimos lo de arriba, pero solo para decenas y unidades
		decenas = luckynum / 10;
		luckynum %= 10;
		unidades = luckynum;

		// machacamos luckynum otra vez
		luckynum = decenas + unidades;

		System.out.println("Tu \"Lucky Number\" personal es "+ luckynum);

    }
}
