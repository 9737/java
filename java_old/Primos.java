public class Primos {
	public static boolean esPrimo(long num) {
		boolean res = true;
		if (num%2==0) res = false;
		for (int i=3; i<=Math.sqrt(num); i+=2) {
			if (num%i==0) res = false;
		}
		return res;
	}
	public static void infoSalida() {
		System.out.println("Programa que reciba un número y lo descomponga "+
				"en el producto de sus factores primos.");
		System.out.println("Uso: java Primos {número}");
		System.exit(1);
	}
	public static void main(String[] args) {
		if (args.length != 1) {
			infoSalida();
		}
		long num = 0;
		try {
			num = Long.parseLong(args[0]);
		}
		catch (NumberFormatException nfe) {
			System.out.println("No es un número entero válido ");
			System.out.println(nfe.getMessage());
		}
		boolean negativo = false;
		if (num < 0) negativo = true;

		while (num%2==0) {
			// para descartar la mitad de los números
			System.out.print("2");
			num /= 2;
			System.out.println("\tResto: "+num);
		}
		for (long j = num; ((j>=3) && (num >1)); j-=2) {
			//test si j es primo
			if (esPrimo(j)) {
				while (num%j==0) {
					System.out.print(j);
					num /=j;		
					System.out.println("\tResto: "+num);

				}
			}
		}
			
	}
}
