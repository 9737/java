public class Ejercicio13
{
    public static void infoSalida()
    {
        System.out.println("Programa que reciba un número, n, e imprima "+
			"los n primeros términos de la sucesión de Fibonacci");
        System.out.println("Uso: java Ejercicio13 n");
        System.exit(1);
    }
    public static int f(int n) {
	if (n==0) {
		return 0;
	}
	else if (n==1) {
		return 1;
	}
	else {
		return f(n-1) + f(n-2);
	}
    }

    public static void main(String args[])
    {
        if (args.length != 1)
        {
            infoSalida();
        }
        int n = 1;
        try
        {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e)
        {
            infoSalida();
        }
        if (n < 2)
        {
            infoSalida();
        }

	// definición fibonacci
	// f_0 = 0, f_1 = 1, f_n = f_n-1 + f_n-2
	System.out.println("F_1 = 1");
        for (int i = 2; i <= n; i++) {
	    System.out.println("F_"+i+" = "+f(i));
	}	
    }
}
