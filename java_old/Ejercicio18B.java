/* package com.mycompany.u02; */

public class Ejercicio18B {
    public static void infoSalida()
    {
        System.out.println("Programa que reciba la Entidad, Sucursal y "+
                       "Cuenta Bancaria y calcule los dígitos de control. ");
        System.out.println("Uso: java Ejercicio18B {entidad} {sucursal} "+
                       "{número cuenta}");
        System.exit(1);
    }

public static long sumaPonderada(long variable_entrada, int[] factores) {
	long res = 0;
	for (int i=factores.length-1;i>-1;i--) {
		res += variable_entrada % 10 * factores[i];
		variable_entrada /= 10;
	}
	return res;
}


	public static long trimfinal(long digito) {
        digito = 11 - digito % 11;
        if (digito == 10) digito = 1;
        else if (digito == 11) digito = 0;
        return digito;
    }

    public static void main(String args[])
    {
        int entidad=1234, sucursal=1234;
        long cuenta=1234567890;
        long digito1, digito2;
        if (args.length != 3)
        {
            infoSalida();
        }
        try
        {
            entidad = Integer.parseInt(args[0]);
            sucursal = Integer.parseInt(args[1]);
            cuenta = Long.parseLong(args[2]);
        }
        catch (NumberFormatException e)
        {
            e.getMessage();
            infoSalida();
        }
        if (entidad > 9999) {
            infoSalida();
        }
        if (sucursal > 9999) {
            infoSalida();
        }
        if (cuenta > 9999999999L) {
            System.err.println("Número de cuenta demasiado largo.");
            infoSalida();
        }

            /* La primera cifra de la entidad se multiplica por 4.
La segunda cifra de la entidad se multiplica por 8.
La tercera cifra de la entidad se multiplica por 5.
La cuarta cifra de la entidad se multiplica por 10.
*/
        System.out.println("Se calcula el número de control para la cuenta: ");
        System.out.println("Entidad: "+entidad);
        System.out.println("Sucursal: "+sucursal);
        System.out.println("Cuenta: "+cuenta);
        
		int factores[] = {4,8,5,10};
	    digito1 = sumaPonderada(entidad, factores);

/*        digito1 = entidad % 10 * 10;
        entidad /= 10;
        digito1 += entidad % 10 * 5;
        entidad /= 10;
        digito1 += entidad % 10 * 8;
        entidad /= 10;
        digito1 += entidad * 4;
*/
        
/* La primera cifra de la oficina se multiplica por 9.
La segunda cifra de la oficina se multiplica por 7.
La tercera cifra de la oficina se multiplica por 3.
La cuarta cifra de la oficina se multiplica por 6.
*/
		factores[0] = 9;
			factores[1] = 7;
   		factores[2] = 3;
		factores[3] = 6;

		digito1 += sumaPonderada(sucursal, factores);

/*		digito1 += sucursal % 10 * 6;
        sucursal /= 10;
        digito1 += sucursal % 10 * 3;
        sucursal /= 10;
        digito1 += sucursal % 10 * 7;
        sucursal /= 10;
        digito1 += sucursal * 9;
*/

 /* Se divide entre 11 y nos quedamos con el resto de la división.
A 11 le quitamos el resto anterior, y ese el el primer dígito de control,
con la salvedad de que si nos da 10, el dígito es 1,
y si nos da 11, el dígito es 0.
*/
        digito1 = trimfinal(digito1);

/* La primera cifra de la cuenta se multiplica por 1
La segunda cifra de la cuenta se multiplica por 2
La tercera cifra de la cuenta se multiplica por 4
La cuarta cifra de la cuenta se multiplica por 8
La quinta cifra de la cuenta se multiplica por 5
La sexta cifra de la cuenta se multiplica por 10
La séptima cifra de la cuenta se multiplica por 9
La octava cifra de la cuenta se multiplica por 7
La novena cifra de la cuenta se multiplica por 3
La décima cifra de la cuenta se multiplica por 6
Se suman todos los resultados obtenidos.
*/

		int factores2[] = {1,2,4,8,5,10,9,7,3,6};
		digito2 = sumaPonderada(cuenta, factores2);
		/*
        digito2 = cuenta % 10 * 6;  
        cuenta /= 10;
        digito2 += cuenta % 10 * 3;
        cuenta /= 10;
        digito2 += cuenta % 10 * 7;
        cuenta /= 10;
        digito2 += cuenta % 10 * 9;
        cuenta /= 10;
        digito2 += cuenta % 10 * 10;
        cuenta /= 10;
        digito2 += cuenta % 10 * 5;
        cuenta /= 10;
        digito2 += cuenta % 10 * 8;
        cuenta /= 10;
        digito2 += cuenta % 10 * 4;
        cuenta /= 10;
        digito2 += cuenta % 10 * 2;
        cuenta /= 10;
        digito2 += cuenta;
        */
/* Se divide entre 11 y nos quedamos con el resto de la división.
A 11 le quitamos el resto anterior, y ese el el segundo dígito de control,
con la salvedad de que si nos da 10, el dígito es 1,
y si nos da 11 el dígito es 0.
*/
        digito2 = trimfinal(digito2);

        System.out.println("El número de control para esta cuenta es "+
                        digito1+""+digito2);
    }        
}
