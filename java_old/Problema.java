public class Problema {
// Programa que reciba un número 
	public static int MAXY = 7;
	public static long codPunto(int x,int y) {
		return (long) Math.pow(2,y*(MAXY-1)+x);
	}
	public static void main(String[] args) {

	// codificar rombo
	// 			pow(2,y*MAXY-1+x)

	long rombo1 =
			codPunto(3,0)+
			codPunto(2,1)+
			codPunto(3,1)+
			codPunto(4,1)+

			codPunto(1,2)+
			codPunto(2,2)+
			codPunto(3,2)+
			codPunto(4,2)+
			codPunto(5,2)+

			codPunto(0,3)+
			codPunto(1,3)+
			codPunto(2,3)+
			codPunto(3,3)+
			codPunto(4,3)+
			codPunto(5,3)+
			codPunto(6,3)+

			codPunto(1,4)+
			codPunto(2,4)+
			codPunto(3,4)+
			codPunto(4,4)+
			codPunto(5,4)+

			codPunto(2,5)+
			codPunto(3,5)+
			codPunto(4,5)+

			codPunto(3,6);
		
		for(int y=0;y<MAXY; y++) {
			for(int x=0;x<MAXY; x++) {
					
					System.err.println("x: "+x+", y: "+y+" => "+codPunto(x,y));
					System.err.println(rombo1+" % "+codPunto(x,y)+" = "+rombo1%codPunto(x,y)); 
					
				if (rombo1%codPunto(x,y)==0) {
					System.out.print("* ");
					rombo1 -= codPunto(x,y);
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}
