public class Ejercicio11 {
    public static void infoSalida() {
        System.out.println("Programa que reciba un número y diga si es primo.");
        System.out.println("Uso: java Ejercicio11 n");
        System.exit(1);
    }
    public static void main(String args[]) {
        if (args.length!=1) {
            infoSalida();
        }
        int n=1;
        try {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e) {
            infoSalida();
        }
        if (n<2) {
            infoSalida();
        }

        int j=n;
        Boolean isPrimo = true;
        if ((j%2==0) && (j!=2)) {
            isPrimo = false;
        } 

        else {               // haz eso solamente para impares
            for (int i=3;i<=Math.sqrt(j);i+=2) {// es j número primo?
                if (j%i==0) {
                    isPrimo = false;
                    break;      // saltar del for en primer ocasión
                }
            }
            if (isPrimo == true) {
                System.out.println(j+" es un número primo.");
            } 
            else {
                System.out.println(j+" no es un número primo.");
            }
        }
    }
}

