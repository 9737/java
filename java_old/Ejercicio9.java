import java.util.Scanner;
public class Ejercicio9 {
    public static void infoSalida() {
        System.out.println("Programa que nos diga cuántos dígitos tiene "+
               " un número entero positivo introducido por teclado.");
        System.out.println("Uso: java Ejercicio9");
        System.exit(1);
    }
    public static void main(String args[]) {

/*
        if (args.length!=1) {
            infoSalida();
        }
        int n=1;
        try {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e) {
            infoSalida();
        }
        if (n<0) {
            infoSalida();
        }
*/
        System.out.println("Programa que nos diga cuántos dígitos tiene "+
               " un número entero positivo introducido por teclado.");
        System.out.print("Introduce un nuúmero entero positivo: ");
        Scanner teclado = new Scanner(System.in);
        long n = 0;
        try {
            n = teclado.nextLong();
        }
        catch ( Exception e) {
            infoSalida();
        }
        if (n<0) {
            infoSalida();
        }

        int contador = 1;
        Boolean tieneDigito = true;

        long decimal = 10;
        do {
            if (n / decimal> 0) {
               contador++;
               decimal*=10; 
            } 
            else {
                tieneDigito = false;
            }
        } while (tieneDigito==true);
        System.out.println(n+" tiene "+contador+" digitos.");
    }
}
