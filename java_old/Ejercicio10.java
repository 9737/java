public class Ejercicio10 {
    public static void infoSalida() {
        System.out.println("Programa que reciba un número entero positivo <= 3999 "+
                "y lo muestre en números Romanos");
            System.out.println("Uso: java Ejercicio10 n");
            System.exit(1);
    }
    
    public static String cuentaRom(int digito, char inicial, char medio, char siguiente) {
        String iniStr, medStr, sigStr;
        iniStr = Character.toString(inicial);
        medStr = Character.toString(medio);
        sigStr = Character.toString(siguiente);
        switch (digito) {
            case 1:
                return iniStr;
            case 2:
                return iniStr + iniStr;
            case 3:
                return iniStr + iniStr + iniStr;
            case 4:
                return iniStr + medStr;
            case 5:
                return medStr;
            case 6: 
                return medStr + iniStr;
            case 7: 
                return medStr + iniStr + iniStr;
            case 8: 
                return medStr + iniStr + iniStr + iniStr;
            case 9: 
                return iniStr + sigStr;
        }
        return "";
    }
    public static void main(String args[]) {
        if (args.length!=1) {
            infoSalida();
        }
        int n=1;
        try {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e) {
            infoSalida();
        }
        if ((n<1) || (n>3999)) {
            infoSalida();
        }

        int miles, centenas, decenas, unidades;
        int resto = n;

        miles = resto / 1000;
        resto = resto % 1000;
        centenas = resto / 100;
        resto = resto % 100;
        decenas = resto / 10;
        resto = resto % 10;
        unidades = resto;

        String resultado = "";

        // Escribir miles
        for (int i=1;i<=miles;i++) {
            resultado += "M";
        }
        resultado += cuentaRom(centenas,'C','D','M');
        resultado += cuentaRom(decenas,'X','L','C');
        resultado += cuentaRom(unidades,'I','V','X');
        
        System.out.println("Número "+n+" en númerales romanos: "+ resultado);

    }
}
