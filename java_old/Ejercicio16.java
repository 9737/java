import java.util.Scanner;
public class Ejercicio16 {
    public static void infoSalida() {
        System.out.println("Programa que reciba un número impar, n, "+
					   "e imprima un rombo de caracteres '*' de diagonal n "+
	   				   " un número entero positivo introducido por teclado.");
        System.out.println("Uso: java Ejercicio16 {n}");
        System.exit(1);
	}
	public static void pintaLinea(int y,int n) {
		for (int x=0;x<n;x++) {
			//pintar mitad +- y
			if ((x>(n/2)-y) && (x<(n/2)+y)) {
				System.out.print("*");
			}
			else {
				System.out.print(" ");
			}
			// (estirar el rombo en dirección x
			// porque distancia entre lineas es más o menos 2 caracteres
			System.out.print(" ");
		}
		System.out.println();
	}
    public static void main(String args[]) {

        if (args.length!=1) {
            infoSalida();
        }
        int n=1;
        try {
            n = Integer.parseInt(args[0]);
        }
        catch (Exception e) {
            infoSalida();
        }
        if (n<0) {
            infoSalida();
        }
		if (n%2==0) {
			infoSalida();
		}

		// travesar cuadro de n*n y decidir si hay falta pintar '*'
		// mitad de arriba
		for (int y=1;y<n/2+1;y++) {
			pintaLinea(y,n);
		}
		// mitad de abajo
		for (int y=n/2+1;y>0;y--) {
			pintaLinea(y,n);
		}
    }
}
