/*
I
	V
		I
		X
		L
		C
		D
		M
	X
		X
			X
		I
		L
		C
		D
		M
	L
		X
		C
		D
		M
	C
		X
		C
			C
		D
		M
	D
		C
		M
	M
		M
V
X
L
c
D
M




*/


public class Ejercicio19 {
	public static void infoSalida() {
		System.out.println("Programa que reciba un número romano y dé su valor en decimal.");
		System.out.println("Uso: java Ejercicio {númeroromano}");
		System.exit(1);
	}
	public static void main(String args[]) {
		if (args.length !=1) {
			infoSalida();
		}
		String rom = args[0];
		int suma;
		int nivel =1; // I:1, V:2, X:3, L:4, C: 5, D:6, M:7;
		for (int i=rom.length-1; i>=0; i--) {
			if ((nivel==1) && (rom[i]=="I")) { // caso "suma"
				suma +=1;
			}
			else if ((nivel==1) && (rom[i]=="V"){ // caso "salto"
				nivel = 2;
				suma += 5;
			}
				// faltan otros saltos

			else if ((nivel==2) && (rom[i])=="I"){	// caso "resta"
				suma -= 1;
				nivel = 3;
			}
			else if ((nivel==2) && (rom[i]=="V")) { // caso "suma"
				suma +=5;
			}
			else if ((nivel==2) && (rom[i]=="X")) { //caso "salto"
				suma +=10;
				nivel = 3;
			}

			else if ((nivel==3) && (rom[i])=="X") { // suma
				suma += 10;
			}
			else if ((nivel==3) && (rom[i]=="I"){ // resta
				nivel = 4;
				suma -= 1;
			}
			else if ((nivel==3) && (rom[i]=="L")) { // salto
				suma += 50;
				nivel = 4;
			}


		}
	}
}